use std::{
    cell::{Cell, RefCell},
    fmt::Display,
    mem,
    rc::Rc,
};
use yew::prelude::*;

use wasm_bindgen_futures::{
    spawn_local,
    wasm_bindgen::{closure::Closure, JsCast as _, JsValue},
    JsFuture,
};
use web_sys::{
    window, AnalyserNode, AudioContext, CanvasRenderingContext2d, HtmlCanvasElement, MediaStream,
    MediaStreamAudioSourceNode, MediaStreamConstraints,
};

fn main() {
    console_error_panic_hook::set_once();
    wasm_logger::init(wasm_logger::Config::default());
    // wasm_bindgen_futures::spawn_local(main_async());

    yew::Renderer::<App>::new().render();
}

#[function_component]
fn App() -> Html {
    let note = use_state(|| 255_f32);
    let frequency = use_state(|| 0f32);
    let playing = use_state(|| false);
    let stop = use_mut_ref(|| false);
    let onclick = {
        let note = note.clone();
        let frequency = frequency.clone();
        let playing = playing.clone();
        let stop = stop.clone();
        move |_| {
            if *playing {
                *stop.borrow_mut() = true;
                playing.set(false);
            } else {
                *stop.borrow_mut() = false;
                spawn_local(the_async_thread(
                    note.clone(),
                    frequency.clone(),
                    stop.clone(),
                ));
                playing.set(true);
            }
        }
    };
    html! {
        <>
            <h1 class="panel"> { "Pitcher" } </h1>
            <div id="canvas_container">
                <canvas id="canvas" width={CANVAS_WIDTH.to_string()} height={CANVAS_HEIGHT.to_string()}/>
                <button {onclick} id="record" class = {classes!(playing.then(|| "playing"))} > { "Record" } </button>
            </div>
            <div class="gizmo_container panel">
                <div class="gizmo_sub_container">
                    <div class="gizmo">
                        <span class="title"> { "Frequency" } </span>
                        <span class="display"> { format!("{:.0}", *frequency) } </span>
                        <span class="unit"> { "Hz" } </span>
                    </div>
                    <span class="gizmo_break"/>
                    <div class="gizmo">
                        <span class = "title"> { "Note" } </span>
                        <span class="display"> {{
                            let note_s = Note(*note as u8);

                            html!{
                                <> {note_s.note_in_octave()} <sub> {note_s.octave()} </sub> </>
                            }
                        }}</span>
                    </div>
                    <div class="gizmo">
                        <span class = "title"> { "Microtones" } </span>
                        <span class="display"> { format!(" {:+.2}", *note - note.round())  } </span>
                    </div>
                </div>
            </div>
        </>
    }
}

#[derive(Debug)]
pub struct AudioInfo {
    pub audio_context: AudioContext,
    pub analyser: AnalyserNode,
    pub nyquist: f32,
    pub data_array: Vec<u8>,
    pub media_stream_source: MediaStreamAudioSourceNode,
    pub canvas_ctx: CanvasRenderingContext2d,
    pub note_handle: UseStateHandle<f32>,
    pub frequency_handle: UseStateHandle<f32>,
    pub stop: Rc<RefCell<bool>>,
    pub killed: bool,
}

const CANVAS_WIDTH: f64 = 500.0 * 3.0;
const CANVAS_HEIGHT: f64 = 300.0 * 3.0;

async fn the_async_thread(
    note_handle: UseStateHandle<f32>,
    frequency_handle: UseStateHandle<f32>,
    stop: Rc<RefCell<bool>>,
) {
    let stream = get_media_recorder().await;
    let canvas: HtmlCanvasElement = window()
        .unwrap()
        .document()
        .unwrap()
        .get_element_by_id("canvas")
        .unwrap()
        .unchecked_into();
    let canvas_ctx = canvas.get_context("2d").unwrap().unwrap().unchecked_into();
    // media_recorder.start_with_time_slice(64).unwrap();

    let audio_context = AudioContext::new().unwrap();
    let analyser = audio_context.create_analyser().unwrap();
    let nyquist = audio_context.sample_rate() / 2.0;
    analyser.set_fft_size(0x8000);

    let buffer_length = analyser.frequency_bin_count() as usize;
    let data_array = vec![0; buffer_length];

    let media_stream_source = audio_context.create_media_stream_source(&stream).unwrap();
    media_stream_source
        .connect_with_audio_node(&analyser)
        .unwrap();

    let audio_info = AudioInfo {
        audio_context,
        analyser,
        nyquist,
        data_array,
        media_stream_source,
        canvas_ctx,
        note_handle,
        frequency_handle,
        stop,
        killed: false,
    };

    let audio_info = Rc::new(RefCell::new(audio_info));

    draw(audio_info);

    println!("Hello, world!");
}

fn draw(info: Rc<RefCell<AudioInfo>>) {
    // Stop if we have to
    if *info.borrow().stop.borrow() {
        return;
    }

    let info_c = info.clone();
    let closure = Closure::<dyn Fn()>::new(move || draw(info_c.clone()));
    window()
        .unwrap()
        .request_animation_frame(closure.as_ref().unchecked_ref())
        .unwrap();
    closure.forget();

    let mut info = info.borrow_mut();
    // request_animation_frame(draw);
    let mut data_array = mem::take(&mut info.data_array);
    info.analyser.get_byte_frequency_data(&mut data_array);
    info.data_array = data_array;

    info.canvas_ctx
        .set_fill_style(&JsValue::from_str("rgb(0, 0, 0)"));
    info.canvas_ctx
        .fill_rect(0.0, 0.0, CANVAS_WIDTH, CANVAS_HEIGHT);

    let bar_width = CANVAS_WIDTH / info.data_array.len() as f64 * 2.5;
    let mut max_val = 0;
    let mut max_index = 0;
    let mut x = 0.0;
    for (i, &bar_height) in info.data_array.iter().enumerate() {
        if bar_height > max_val {
            max_val = bar_height;
            max_index = i;
        }

        let bar_height = bar_height as f64;
        info.canvas_ctx.set_fill_style(&JsValue::from_str(&format!(
            "rgb({}, {}, {})",
            bar_height as f32 * 4.0,
            bar_height as f32 * 2.0,
            bar_height as f32 * 0.5,
        )));
        info.canvas_ctx.fill_rect(
            x,
            CANVAS_HEIGHT * (1.0 - (bar_height / 255.0)),
            bar_width,
            bar_height * CANVAS_HEIGHT,
        );
        x += bar_width;
    }
    let freq = max_index as f32 * info.nyquist / info.data_array.len() as f32;
    let mut note = 12.0 * (freq / 440.0).log2() + 49.0;
    if note.is_nan() {
        note = 0.0;
    }

    info.frequency_handle.set(freq);
    info.note_handle.set(note);
}

async fn get_media_recorder() -> MediaStream {
    let window = window().unwrap();
    let navigator = window.navigator();
    let media_devices = navigator.media_devices().unwrap();
    let stream = media_devices
        .get_user_media_with_constraints(&MediaStreamConstraints::new().audio(&true.into()))
        .unwrap();
    let stream = JsFuture::from(stream).await.unwrap();
    let stream: MediaStream = stream.try_into().unwrap();
    stream
    // MediaRecorder::new_with_media_stream(&stream).unwrap()
}

pub struct Note(pub u8);

impl Note {
    fn note_in_octave(&self) -> &'static str {
        [
            "A",
            "A♯/B♭",
            "B",
            "C",
            "C♯/D♭",
            "D",
            "D♯/E♭",
            "E",
            "F",
            "F♯/G♭",
            "G",
            "G♯/A♭",
        ][(self.0 as usize + 11) % 12]
    }

    fn octave(&self) -> usize {
        self.0 as usize / 12
    }
}
